// ignore_for_file: depend_on_referenced_packages

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:hive/hive.dart';
import 'package:salesperson/Language/language.dart';
import 'package:salesperson/constant/pages.dart';
import 'package:path_provider/path_provider.dart' as path_provider;

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  final appDocumentDir = await path_provider.getApplicationDocumentsDirectory();
  Hive.init(appDocumentDir.path);
  runApp(ScreenUtilInit(
      useInheritedMediaQuery: true,
      designSize: const Size(540, 960),
      minTextAdapt: true,
      builder: (BuildContext context, _) {
        SystemChrome.setPreferredOrientations([
          DeviceOrientation.portraitUp,
        ]);
        SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(
          statusBarColor: Color(0xFF083c53),
        ));
        return GetMaterialApp(
            initialRoute: '/',
            debugShowCheckedModeBanner: false,
            defaultTransition: Transition.native,
            translations: MyTranslations(),
            locale: const Locale('pt', 'BR'),
            getPages: listOfPages);
      }));

}
