import 'dart:developer';

import 'package:hive/hive.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:salesperson/API/AppUrls.dart';
import 'package:shared_preferences/shared_preferences.dart';

@HiveType(typeId: 0)
class UserModel extends HiveObject {
  @HiveField(0)
  late String? id;

  @HiveField(1)
  late String? userName;

  @HiveField(2)
  late String? password;

  @HiveField(3)
  late String? isStaff;

  @HiveField(4)
  late String? jobNumber;

  @HiveField(5)
  late String? image;

  @HiveField(6)
  late String? region;

  UserModel({
    this.id,
    this.userName,
    this.password,
    this.isStaff,
    this.jobNumber,
    this.image,
    this.region,
  });

//################### Sign In

  static Future<Object?> signIn(String username, String password) async {
    final url = Uri.parse(AppURL.SignIn);

    final response = await http.post(
      url,
      body: {
        'username': username,
        'password': password,
      },
    );

    if (response.statusCode == 200) {
      log("User LoggedIn");
      final data = json.decode(response.body);
      final user = UserModel(
        id: data['id'],
        userName: data['user_name'],
        password: password,
        isStaff: data['is_staff'],
        jobNumber: '',
        image: '',
        region: null,
      );
      saveUserModel(user);
      // Save the user object to Hive
      // final userBox = await Hive.openBox<UserModel>('users');
      // await userBox.put('current_user', user);

      // return user;
      return await getUserModel();
    } else {
      return "error and not 200";
    }
  }

//################### Sign Up

  static Future<UserModel> createUser(String username, String password,
      String jobNumber, String image, int region) async {
    final url = Uri.parse(
        'https://your-api-url.com/createUser'); // Replace with your actual API endpoint

    final response = await http.post(
      url,
      body: {
        'username': username,
        'password': password,
        'job_number': jobNumber,
        'image': image,
        'region': region.toString(),
      },
    );

    if (response.statusCode == 200) {
      final data = json.decode(response.body);
      final user = UserModel(
        id: data['id'],
        userName: data['username'],
        password: password,
        jobNumber: data['job_number'],
        image: data['image'],
        region: data['region'],
      );

      // // Save the user object to Hive
      // final userBox = await Hive.openBox<User>('users');
      // await userBox.put('current_user', user);

      return user;
    } else {
      throw Exception('Failed to create user');
    }
  }

//################### modify User

  static Future<void> modifyUser(
      {required int id,
      required String username,
      required String jobNumber,
      String? image,
      required String region}) async {
    final url = Uri.parse('${AppURL.Edit}$id/');

    final response = await http.patch(
      url,
      headers: {
        'Content-Type': 'application/json',
      },
      body: json.encode({
        'id': id.toString(),
        'username': username,
        'job_number': jobNumber,
        'image': image,
        'region': region.toString(),
      }),
    );

    if (response.statusCode != 200) {
      throw Exception('Failed to modify user');
    }
  }

//################### Delete User

  static Future<void> deleteUser(int id) async {
    final url = Uri.parse('${AppURL.Delete}$id/');

    final response = await http.delete(url);

    if (response.statusCode != 204) {
      throw Exception('Failed to delete user');
    }
  }

//################### Show User info

  static Future<Map<String, dynamic>> getUserInfo(int id) async {
    final url = Uri.parse('${AppURL.UserInfo}$id/');

    final response = await http.get(url);

    if (response.statusCode == 200) {
      final data = json.decode(response.body);
      return {
        'id': data['id'],
        'username': data['username'],
        'job_number': data['job_number'],
        'image': data['image'],
        'region': data['region'],
      };
    } else {
      throw Exception('Failed to get user info');
    }
  }

  static void saveUserModel(UserModel userModel) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    // Save the UserModel properties individually
    prefs.setInt('id', userModel.id as int);
    prefs.setString('userName', userModel.userName ?? "");
    prefs.setString('password', userModel.password ?? "");
    prefs.setBool('isStaff', userModel.isStaff as bool);
    prefs.setString('jobNumber', userModel.jobNumber ?? '');
    prefs.setString('image', userModel.image ?? '');
    prefs.setString('region', userModel.region ?? '');
  }

  static Future<bool> isUserStaff() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    int? id = prefs.getInt('id');
    String? userName = prefs.getString('userName');
    String? password = prefs.getString('password');
    bool? isStaff = prefs.getBool('isStaff');

    if (id != null && userName != null && password != null && isStaff != null) {
      print("Hello admin");

      return isStaff;
    }
    print("there isn't any user");
    return false;
  }

  static Future<UserModel?> getUserModel() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    int? id = prefs.getInt('id');
    String? userName = prefs.getString('userName');
    String? password = prefs.getString('password');
    bool? isStaff = prefs.getBool('isStaff');
    String? jobNumber = prefs.getString('jobNumber');
    String? image = prefs.getString('image');
    String? region = prefs.getString('region');

    if (id != null && userName != null && password != null) {
      return UserModel(
        id: id.toString(),
        userName: userName,
        password: password,
        isStaff: isStaff.toString(),
        jobNumber: jobNumber,
        image: image,
        region: region,
      );
    }

    return null;
  }
}


// To retrieve the saved user object from Hive, you can use the following code:

// final userBox = await Hive.openBox<User>('users');
// final currentUser = userBox.get('current_user');




  // static void saveUserModel(UserModel userModel) async {
  //   final box = await Hive.openBox('userBox'); // Open the Hive box

  //   // Save the userModel object in the Hive box
  //   await box.put('user', userModel);

  //   // Close the Hive box
  //   await box.close();
  // }

  // static UserModel? getUserModel() {
  //   final box = Hive.box('userBox');
  //   return box.get('user');
  // }