class Salesperson {
  int? id;
  String? name;
  String? region;
  String? photo;

  Salesperson(
      {required this.id,
      required this.name,
      required this.region,
      String? photo});
}
