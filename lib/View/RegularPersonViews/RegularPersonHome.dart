// ignore_for_file: prefer_const_constructors, depend_on_referenced_packages, file_names

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:salesperson/View/Widgets/app_colors.dart';
import 'package:salesperson/View/Widgets/app_images.dart';
import 'package:salesperson/View/RegularPersonViews/commission_page.dart';

class RegularPersonHome extends StatelessWidget {
  final String userPicture;
  final String userName;
  final String userRegion;
  final String userNumber;

  const RegularPersonHome({
    super.key,
    required this.userPicture,
    required this.userName,
    required this.userRegion,
    required this.userNumber,
  });

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage(AppPics.getRandomColor()),
                fit: BoxFit.cover,
                opacity: 0.2),
          ),
          child: Center(
            child: Padding(
              padding: const EdgeInsets.all(16.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  CircleAvatar(
                    backgroundColor: AppColors.thirdColor,
                    radius: 80.0,
                    backgroundImage: AssetImage(userPicture),
                  ),
                  const SizedBox(height: 16.0),
                  Text(
                    "Welcome Back",
                    style: TextStyle(
                      fontFamily: 'OpenSans',
                      // fontSize: 24.0,
                      // fontWeight: FontWeight.bold,
                    ),
                  ),
                  Text(
                    userName,
                    style: const TextStyle(
                      fontFamily: 'OpenSans',
                      fontSize: 24.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  const SizedBox(height: 8.0),
                  Text(
                    'Region: $userRegion',
                    style:
                        const TextStyle(fontFamily: 'OpenSans', fontSize: 16.0),
                  ),
                  const SizedBox(height: 8.0),
                  Text(
                    'ID: $userNumber',
                    style:
                        const TextStyle(fontFamily: 'OpenSans', fontSize: 16.0),
                  ),
                  const SizedBox(height: 32.0),
                  OutlinedButton(
                      style: ButtonStyle(
                          backgroundColor: MaterialStateProperty.all<Color>(
                              AppColors.getRandomColor())),
                      onPressed: () {
                        Get.to(CommissionPage());
                      },
                      child: Text(
                        "Add New Record",
                        style: TextStyle(
                            fontFamily: 'OpenSans',
                            color: AppColors.whiteColor),
                      )),

                  /*ElevatedButton(
                    onPressed: () {
                      Get.to(CommissionPage());
                    },
                    child: const Text('Add New Record'),
                  ),*/
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
