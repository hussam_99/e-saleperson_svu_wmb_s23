// ignore_for_file: depend_on_referenced_packages, must_be_immutable

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:salesperson/Controller/user_controller.dart';
import 'package:salesperson/View/Widgets/app_images.dart';
import 'package:salesperson/View/pages/score_page.dart';

class CommissionPage extends GetView<UserController> {
  final controller = Get.put(UserController());
  final TextEditingController salespersonNameController =
      TextEditingController();
  final TextEditingController salespersonNumberController =
      TextEditingController();
  final TextEditingController regionController = TextEditingController();
  final TextEditingController monthController = TextEditingController();
  final TextEditingController yearController = TextEditingController();
  final TextEditingController registrationDateController =
      TextEditingController();
  final TextEditingController southernRegionController =
      TextEditingController();
  final TextEditingController coastalRegionController = TextEditingController();
  final TextEditingController northernRegionController =
      TextEditingController();
  final TextEditingController easternRegionController = TextEditingController();
  final TextEditingController lebanonController = TextEditingController();
  final TextEditingController monthlyCommissionController =
      TextEditingController();
  RxString selectedRegion = ''.obs;

  CommissionPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage(AppPics.getRandomColor()),
              fit: BoxFit.cover,
              opacity: 0.2),
        ),
        child: Padding(
          padding: const EdgeInsets.all(16),
          child: ListView(
            children: [
              const Text(
                'Salesperson Information',
                style: TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.bold,
                ),
              ),
              const SizedBox(height: 10),
              buildTextField('Salesperson Name', salespersonNameController),
              const SizedBox(height: 10),
              buildNumericTextField(
                  'Salesperson Number', salespersonNumberController),
              const SizedBox(height: 20),
              const Text(
                'Commission Details',
                style: TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.bold,
                ),
              ),
              const SizedBox(height: 10),
              buildTextField('Region', regionController),
              const SizedBox(height: 10),
              buildNumericTextField('Month', monthController),
              const SizedBox(height: 10),
              buildNumericTextField('Year', yearController),
              const SizedBox(height: 10),
              buildTextField('Registration Date', registrationDateController),
              const SizedBox(height: 10),
              buildNumericTextField(
                  'Southern Region', southernRegionController),
              const SizedBox(height: 10),
              buildNumericTextField('Coastal Region', coastalRegionController),
              const SizedBox(height: 10),
              buildNumericTextField(
                  'Northern Region', northernRegionController),
              const SizedBox(height: 10),
              buildNumericTextField('Eastern Region', easternRegionController),
              const SizedBox(height: 10),
              buildNumericTextField('Lebanon', lebanonController),
              const SizedBox(height: 10),
              buildResultTextField(
                  'Monthly Commission', monthlyCommissionController),
              const SizedBox(height: 20),
              ElevatedButton(
                onPressed: () {
                  calculateCommission();
                },
                child: const Text('Calculate Commission'),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget buildTextField(String label, TextEditingController controller) {
    return Row(
      children: [
        Expanded(
          child: TextField(
            controller: controller,
            decoration: InputDecoration(
              labelText: label,
              border: const OutlineInputBorder(),
            ),
          ),
        ),
        IconButton(
          icon: const Icon(Icons.map),
          onPressed: () {
            // openMap(controller);
          },
        ),
      ],
    );
  }

  Widget buildNumericTextField(String label, TextEditingController controller) {
    return TextField(
      controller: controller,
      keyboardType: TextInputType.number,
      inputFormatters: <TextInputFormatter>[
        FilteringTextInputFormatter.digitsOnly,
      ],
      decoration: InputDecoration(
        labelText: label,
        border: const OutlineInputBorder(),
      ),
    );
  }

  Widget buildResultTextField(String label, TextEditingController controller) {
    return TextField(
      controller: controller,
      enabled: false,
      decoration: InputDecoration(
        labelText: label,
        border: const OutlineInputBorder(),
      ),
    );
  }

  void calculateCommission() {
    double southernRegion = double.tryParse(southernRegionController.text) ?? 0;
    double coastalRegion = double.tryParse(coastalRegionController.text) ?? 0;
    double lebanon = double.tryParse(lebanonController.text) ?? 0;

    double commission = (southernRegion > 1000000
            ? 1000000 * 0.05 + (southernRegion - 1000000) * 0.07
            : southernRegion * 0.05) +
        coastalRegion * 0.03 +
        lebanon * 0.03;

    double monthlyCommission =
        commission + coastalRegion * 0.03 + lebanon * 0.03;

    monthlyCommissionController.text = monthlyCommission.toStringAsFixed(2);
    Get.to(ScorePage(
      score: monthlyCommission,
      salespersonNumber: salespersonNumberController.text,
      salespersonName: salespersonNameController.text,
      month: monthController.text,
      year: yearController.text,
      registrationDate: registrationDateController.text,
      southernRegion: southernRegionController.text,
      coastalRegion: coastalRegionController.text,
      northernRegion: northernRegionController.text,
      easternRegion: easternRegionController.text,
      lebanon: lebanonController.text,
      monthlyCommission: monthlyCommissionController.text,
    ));
    //Get.to(ScorePage(score: monthlyCommission));
  }
}
