// ignore_for_file: file_names

import 'package:flutter/material.dart';

class NoData extends StatelessWidget {
  const NoData({super.key});

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      body: Center(child: Image.asset("assets/images/noData.png")),
    ));
  }
}
