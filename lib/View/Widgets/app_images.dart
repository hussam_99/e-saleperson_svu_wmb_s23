import 'dart:math';

class AppPics {
  static List<String> mainBgs = [
    "assets/images/bg/bg1.jpg",
    "assets/images/bg/bg2.jpg",
    "assets/images/bg/bg3.jpg",
    "assets/images/bg/bg4.jpg",
    "assets/images/bg/bg5.jpg",
    "assets/images/bg/bg6.jpg",
    "assets/images/bg/bg7.jpg",
  ];

  static String getRandomColor() {
    Random random = Random();
    int index = random.nextInt(mainBgs.length);
    return mainBgs[index];
  }
}
