// ignore_for_file: depend_on_referenced_packages, prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:salesperson/Controller/user_controller.dart';
import 'package:salesperson/View/Widgets/app_colors.dart';
import 'package:salesperson/View/Widgets/input_field.dart';
import 'package:salesperson/View/pages/homePage.dart';
import 'package:salesperson/constant/app_route.dart';

class SignUpView extends GetView<UserController> {
  SignUpView({super.key});
  @override
  final controller = Get.put(UserController());

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
          body: Stack(
        children: [
          Container(
            decoration: const BoxDecoration(
              image: DecorationImage(
                image: AssetImage("assets/images/signup/signuppage.png"),
                fit: BoxFit.cover,
              ),
            ),
            child: Center(
                child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(
                  height: 300.h,
                ),
                rowWidget(),
                buttonWidget(),
              ],
            )),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Align(
              alignment: Alignment.bottomLeft,
              child: InkWell(
                onTap: () {
                  Get.offAllNamed(AppRoute.login);
                },
                child: CircleAvatar(
                  radius: 30,
                  backgroundColor: AppColors.getRandomColor(),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Icon(
                        Icons.arrow_back,
                        color: AppColors.whiteColor,
                      ),
                    ],
                  ),
                ),
              ),
            ),
          )
        ],
      )),
    );
  }

  rowWidget() {
    return Column(
      children: [
        Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 8, vertical: 16),
                child: Row(
                  children: [
                    SizedBox(
                        width: 400.w,
                        height: 50.h,
                        child: InputField(
                          label: "User Name:",
                          color: Colors.white,
                          borderRadius: 10.r,
                          controller: controller.signUpUserNameController,
                        ))
                  ],
                ),
              )
            ]),
        Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 8, vertical: 16),
                child: Row(
                  children: [
                    SizedBox(
                        width: 400.w,
                        height: 50.h,
                        child: InputField(
                          label: "Password:",
                          color: Colors.white,
                          borderRadius: 10.r,
                          controller: controller.signUpPasswordNameController,
                        ))
                  ],
                ),
              )
            ]),
        Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 8, vertical: 16),
                child: Row(
                  children: [
                    SizedBox(
                        width: 400.w,
                        height: 50.h,
                        child: InputField(
                          label: "Confirm Password:",
                          color: Colors.white,
                          borderRadius: 10.r,
                          controller:
                              controller.signUpConfirmPasswordNameController,
                        ))
                  ],
                ),
              )
            ]),
      ],
    );
  }

  buttonWidget() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          OutlinedButton(
              style: ButtonStyle(
                  backgroundColor:
                      MaterialStateProperty.all<Color>(AppColors.thirdColor)),
              onPressed: () {
                //Get.toNamed(AppRoute.enterNewEmp);
                // Get.to(DashBoard());
                Get.to(HomePage());
              },
              child: Text(
                "SignUp",
                style: TextStyle(
                    fontFamily: 'OpenSans', color: AppColors.mainColor),
              )),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: SizedBox(
              width: 300.w,
              height: 50.h,
              child: Image.asset("assets/images/signup/google_logo.png"),
            ),
          )
        ],
      ),
    );
  }
}
