// ignore_for_file: depend_on_referenced_packages

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:salesperson/Controller/user_controller.dart';

// ignore: must_be_immutable
class UserInfoView extends GetView<UserController> {
  UserInfoView({super.key});
  @override
  UserController controller = Get.put(UserController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('User Info'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            const CircleAvatar(
              radius: 60,
              backgroundImage: AssetImage('assets/user_placeholder.png'),
            ),
            const SizedBox(height: 16.0),
            ElevatedButton(
              onPressed: () {
                // Implement logic to select and update user photo
              },
              child: const Text('Select Photo'),
            ),
            const SizedBox(height: 16.0),
            TextFormField(
              controller: controller.nameController,
              decoration: const InputDecoration(
                labelText: 'Name',
              ),
            ),
            const SizedBox(height: 16.0),
            TextFormField(
              controller: controller.regionController,
              decoration: const InputDecoration(
                labelText: 'Region',
              ),
            ),
            const SizedBox(height: 16.0),
            TextFormField(
              controller: controller.uniqueNumberController,
              decoration: const InputDecoration(
                labelText: 'Unique Number',
              ),
            ),
            const SizedBox(height: 32.0),
            ElevatedButton(
              onPressed: () {
                // Implement logic to save user info
              },
              child: const Text('Save'),
            ),
          ],
        ),
      ),
    );
  }
}
