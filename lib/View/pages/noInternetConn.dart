import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:salesperson/Controller/SplashController.dart';

class NoInternetConn extends GetView<SplashController> {
  const NoInternetConn({super.key});

  @override
  Widget build(BuildContext context) {
    return const SafeArea(
        child: Scaffold(
      body: Center(
        child: Text("No Internet"),
      ),
    ));
  }
}
