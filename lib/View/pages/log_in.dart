// ignore_for_file: depend_on_referenced_packages

import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:salesperson/Controller/user_controller.dart';
import 'package:salesperson/Model/user_model.dart';
import 'package:salesperson/View/Widgets/app_colors.dart';
import 'package:salesperson/View/Widgets/input_field.dart';
import 'package:salesperson/constant/app_route.dart';

class LoginPage extends GetView<UserController> {
  LoginPage({super.key});
  @override
  final controller = Get.put(UserController());

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
          body: Stack(
        children: [
          Container(
            decoration: const BoxDecoration(
              image: DecorationImage(
                image: AssetImage("assets/images/loginpagebg.png"),
                fit: BoxFit.cover,
              ),
            ),
            child: Center(
                child: SingleChildScrollView(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SizedBox(
                    height: 250.h,
                  ),
                  rowWidget(),
                  buttonWidget(),
                ],
              ),
            )),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Align(
              alignment: Alignment.bottomRight,
              child: InkWell(
                onTap: () {
                  Get.offAllNamed(AppRoute.home);
                },
                child: CircleAvatar(
                  radius: 30,
                  backgroundColor: AppColors.getRandomColor(),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Icon(
                        Icons.arrow_forward,
                        color: AppColors.whiteColor,
                      ),
                    ],
                  ),
                ),
              ),
            ),
          )
        ],
      )),
    );
  }

  rowWidget() {
    return Column(
      children: [
        Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 8, vertical: 16),
                child: Row(
                  children: [
                    SizedBox(
                        width: 400.w,
                        height: 50.h,
                        child: InputField(
                          label: "User Name:",
                          color: Colors.white,
                          borderRadius: 10.r,
                          controller: controller.logInUserNameController,
                        )),
                  ],
                ),
              )
            ]),
        Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 8, vertical: 16),
                child: Row(
                  children: [
                    SizedBox(
                        width: 400.w,
                        height: 50.h,
                        child: InputField(
                          label: "Password:",
                          color: Colors.white,
                          borderRadius: 10.r,
                          controller: controller.logInPasswordNameController,
                        )),
                  ],
                ),
              )
            ]),
      ],
    );
  }

  buttonWidget() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Obx(
            () => controller.isLoading.isTrue
                ? const CircularProgressIndicator() // Show circular indicator while loading
                : OutlinedButton(
                    style: ButtonStyle(
                      backgroundColor: MaterialStateProperty.all<Color>(
                          AppColors.thirdColor),
                    ),
                    onPressed: () async {
                      log("signin");
                      var res = await controller.signIn();
                      if (res == "success") {
                        showMessage(res.toString());
                        Get.toNamed(AppRoute.home);
                        print(UserModel.getUserModel());
                      } else {
                        showMessage(res.toString());
                      }
                    },
                    child: Text(
                      "SignIn",
                      style: TextStyle(
                        fontFamily: 'OpenSans',
                        color: AppColors.mainColor,
                      ),
                    ),
                  ),
          ),

          Padding(
            padding: const EdgeInsets.all(8.0),
            child: InkWell(
              onTap: () {
                Get.toNamed(AppRoute.sigup);
              },
              child: Text(
                "Didn't Have Account?",
                style: TextStyle(
                  fontFamily: 'OpenSans',
                  color: AppColors.mainColor,
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: InkWell(
              onTap: () {
                Get.snackbar("Hi", "Google isn't available at the moment");
              },
              child: SizedBox(
                width: 300.w,
                height: 75.h,
                child: Image.asset("assets/images/signup/google_logo.png"),
              ),
            ),
          ),
        ],
      ),
    );
  }

/*
  buttonWidget() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          OutlinedButton(
              style: ButtonStyle(
                  backgroundColor:
                      MaterialStateProperty.all<Color>(AppColors.thirdColor)),
              onPressed: () async {
                log("signin");
                var res = await controller.signIn();
                if (res == "success") {
                  showMessage(res.toString());
                  Get.toNamed(AppRoute.home);

                  print(UserModel.getUserModel());
                } else {
                  showMessage(res.toString());
                }
              },
              child: Text(
                "SignIn",
                style: TextStyle(
                    fontFamily: 'OpenSans', color: AppColors.mainColor),
              )),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: InkWell(
              onTap: () {
                Get.toNamed(AppRoute.sigup);
              },
              child: Text("Did'nt Have Account?",
                  style: TextStyle(
                      fontFamily: 'OpenSans', color: AppColors.mainColor)),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: InkWell(
              onTap: () {
                Get.snackbar("Hi", "Google is'nt availble in the current time");
              },
              child: SizedBox(
                width: 300.w,
                height: 75.h,
                child: Image.asset("assets/images/signup/google_logo.png"),
              ),
            ),
          )
        ],
      ),
    );
  }
*/
  void showMessage(String text) {
    Get.snackbar("sign In", text);
  }
}
