// ignore: file_names
// ignore_for_file: depend_on_referenced_packages, file_names, prefer_const_constructors, duplicate_ignore

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:salesperson/Controller/user_controller.dart';
import 'package:salesperson/View/RegularPersonViews/RegularPersonHome.dart';
import 'package:salesperson/View/Widgets/app_colors.dart';
import 'package:salesperson/View/administratorViews/dashboard.dart';

class HomePage extends GetView<UserController> {
  HomePage({super.key});

  final controller = Get.put(UserController());
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Container(
          decoration: const BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/images/bg.png"),
              fit: BoxFit.cover,
            ),
          ),
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Column(
                  children: [
                    Text("Welcome Back",
                        style: TextStyle(
                            fontSize: 40.sp,
                            fontFamily: 'OpenSans',
                            color: AppColors.mainColor)),
                    SizedBox(
                      height: 10.h,
                    ),
                    Text("E-SalePerson Management System",
                        style: TextStyle(
                            fontSize: 20.sp,
                            fontFamily: 'OpenSans',
                            color: AppColors.mainColor)),
                  ],
                ),
                Column(
                  children: [
                    GetBuilder<UserController>(builder: (context) {
                      return OutlinedButton(
                          style: ButtonStyle(
                              backgroundColor: MaterialStateProperty.all<Color>(
                                  AppColors.thirdColor)),
                          onPressed: () {
                            Get.to(const DashBoard());
                          },
                          child: Text(
                            "Admin Dashboard",
                            style: TextStyle(
                                fontFamily: 'OpenSans',
                                color: AppColors.mainColor),
                          ));
                    }),
                    const SizedBox(height: 16.0),
                    OutlinedButton(
                        style: ButtonStyle(
                            backgroundColor: MaterialStateProperty.all<Color>(
                                AppColors.thirdColor)),
                        onPressed: () {
                          Get.to(RegularPersonHome(
                            userName: "Hussam",
                            userNumber: "386",
                            userPicture: "assets/hussam.jpg",
                            userRegion: "Lebanon",
                          ));
                        },
                        child: Text(
                          'Regular User Page',
                          style: TextStyle(
                              fontFamily: 'OpenSans',
                              color: AppColors.mainColor),
                        )),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
