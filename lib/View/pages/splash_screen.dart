// ignore_for_file: depend_on_referenced_packages

import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:salesperson/Controller/SplashController.dart';
import 'package:salesperson/View/Widgets/app_images.dart';
import 'package:flutter/material.dart';

class SplashScreen extends GetView<SplashController> {
  @override
  final controller = Get.put(SplashController());
  SplashScreen({super.key});
  @override
  Widget build(BuildContext context) {
    // Future.delayed(const Duration(seconds: 5), () async {
    //   // Navigate to login page after 5 seconds
    //   if (await controller.checkInternetConnection()) {
    //   await UserModel.isUserStaff()
    //       ? Get.offNamed(AppRoute.dashboard)
    //       : Get.offNamed(AppRoute.home);
    // } else {
    //   Get.to(const NoInternetConn());
    // }
    // });
    return Scaffold(
      backgroundColor: Colors.white,
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage(AppPics.getRandomColor()),
              fit: BoxFit.cover,
              opacity: 0.2),
        ),
        child: const Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Spacer(),
              Padding(
                padding: EdgeInsets.all(8.0),
                child: AnimatedLogo(),
              ),
              Padding(
                padding: EdgeInsets.all(8.0),
                child: AnimatedText(),
              ),
              Padding(
                padding: EdgeInsets.all(8.0),
                child: CircularProgressIndicator(),
              ),
              Spacer(),
              AnimatedSVULogo()
            ],
          ),
        ),
      ),
    );
  }
}

class AnimatedLogo extends StatelessWidget {
  const AnimatedLogo({super.key});

  @override
  Widget build(BuildContext context) {
    return GetBuilder<SplashController>(
      init: SplashController(),
      builder: (controller) {
        return Opacity(
            opacity: controller.logoOpacity,
            child: SizedBox(
                width: 250.w,
                height: 250.w,
                child: Image.asset(
                    "assets/images/logo/logo.png")) //FlutterLogo(size: 150),
            );
      },
    );
  }
}

class AnimatedText extends StatelessWidget {
  const AnimatedText({super.key});

  @override
  Widget build(BuildContext context) {
    return GetBuilder<SplashController>(
      init: SplashController(),
      builder: (controller) {
        return Opacity(
          opacity: controller.textOpacity,
          child: const Text(
            'E-Sale Person',
            style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
          ),
        );
      },
    );
  }
}

class AnimatedSVULogo extends StatelessWidget {
  const AnimatedSVULogo({super.key});

  @override
  Widget build(BuildContext context) {
    return GetBuilder<SplashController>(
      init: SplashController(),
      builder: (controller) {
        return Opacity(
            opacity: controller.logoOpacity,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(
                    width: 150.w,
                    height: 150.w,
                    child: Image.asset("assets/images/logo/svu_logo.png")),
              ],
            ) //FlutterLogo(size: 150),
            );
      },
    );
  }
}
