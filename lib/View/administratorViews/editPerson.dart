// ignore_for_file: file_names

import 'package:flutter/material.dart';
import 'package:get/get.dart';

class Salesperson {
  final String id;
  String name;
  String region;

  Salesperson({required this.id, required this.name, required this.region});
}

class DashboardView extends StatelessWidget {
  final List<Salesperson> salespersons = [
    Salesperson(id: '1', name: 'John Doe', region: 'Region A'),
    Salesperson(id: '2', name: 'Jane Smith', region: 'Region B'),
    Salesperson(id: '3', name: 'Mike Johnson', region: 'Region C'),
  ];

  DashboardView({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Dashboard'),
      ),
      body: ListView.builder(
        itemCount: salespersons.length,
        itemBuilder: (context, index) {
          Salesperson salesperson = salespersons[index];
          return ListTile(
            title: Text(salesperson.name),
            subtitle: Text(salesperson.region),
            trailing: IconButton(
              icon: const Icon(Icons.delete),
              onPressed: () {
                // Delete salesperson
                Get.defaultDialog(
                  title: 'Delete Salesperson',
                  content: Text(
                      'Are you sure you want to delete ${salesperson.name}?'),
                  confirm: ElevatedButton(
                    onPressed: () {
                      salespersons.removeAt(index);
                      Get.back();
                    },
                    child: const Text('Delete'),
                  ),
                  cancel: ElevatedButton(
                    onPressed: () {
                      Get.back();
                    },
                    child: const Text('Cancel'),
                  ),
                );
              },
            ),
            onTap: () {
              // Update salesperson information
              Get.defaultDialog(
                title: 'Update Salesperson',
                content: Column(
                  children: [
                    TextFormField(
                      initialValue: salesperson.name,
                      onChanged: (value) {
                        salesperson.name = value;
                      },
                      decoration: const InputDecoration(
                        labelText: 'Name',
                      ),
                    ),
                    TextFormField(
                      initialValue: salesperson.region,
                      onChanged: (value) {
                        salesperson.region = value;
                      },
                      decoration: const InputDecoration(
                        labelText: 'Region',
                      ),
                    ),
                  ],
                ),
                confirm: ElevatedButton(
                  onPressed: () {
                    Get.back();
                  },
                  child: const Text('Update'),
                ),
                cancel: ElevatedButton(
                  onPressed: () {
                    Get.back();
                  },
                  child: const Text('Cancel'),
                ),
              );
            },
          );
        },
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          // Add new salesperson
          Get.defaultDialog(
            title: 'Add Salesperson',
            content: Column(
              children: [
                TextFormField(
                  onChanged: (value) {
                    // Handle new salesperson name
                  },
                  decoration: const InputDecoration(
                    labelText: 'Name',
                  ),
                ),
                TextFormField(
                  onChanged: (value) {
                    // Handle new salesperson region
                  },
                  decoration: const InputDecoration(
                    labelText: 'Region',
                  ),
                ),
              ],
            ),
            confirm: ElevatedButton(
              onPressed: () {
                // Handle adding new salesperson to the list
                Get.back();
              },
              child: const Text('Add'),
            ),
            cancel: ElevatedButton(
              onPressed: () {
                Get.back();
              },
              child: const Text('Cancel'),
            ),
          );
        },
        child: const Icon(Icons.add),
      ),
    );
  }
}
