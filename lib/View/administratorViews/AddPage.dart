// ignore_for_file: avoid_print, file_names, depend_on_referenced_packages

import 'dart:io';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:salesperson/Controller/user_controller.dart';
import 'package:salesperson/Model/sales_person_model.dart';

class AddPage extends GetView<UserController> {
  @override
  final controller = UserController();
  final _formKey = GlobalKey<FormState>();
  final _salesperson = Salesperson(id: 0, name: '', photo: '', region: '');
  AddPage({super.key});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Form(
          key: _formKey,
          child: GetBuilder<UserController>(builder: (_) {
            return controller.isLoading.isFalse
                ? ListView(
                    children: [
                      GetBuilder<UserController>(builder: (_) {
                        return GestureDetector(
                          onTap: pickImage,
                          child: CircleAvatar(
                            radius: 50,
                            backgroundColor: Colors.grey,
                            backgroundImage: controller.image != null
                                ? FileImage(controller.image!)
                                : null,
                            child: controller.image == null
                                ? Icon(
                                    Icons.person,
                                    size: 50,
                                    color: Colors.white,
                                  )
                                : null,
                          ),
                        );
                      }),
                      TextFormField(
                        controller: controller.addJobNumber,
                        decoration:
                            const InputDecoration(labelText: 'Job Number'),
                        keyboardType: TextInputType.number,
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'Please enter a number';
                          }
                          return null;
                        },
                        onSaved: (value) {
                          _salesperson.id = int.parse(value!);
                        },
                      ),
                      TextFormField(
                        controller: controller.addUsername,
                        decoration:
                            const InputDecoration(labelText: 'User Name'),
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'Please enter a name';
                          }
                          return null;
                        },
                        onSaved: (value) {
                          _salesperson.name = value!;
                        },
                      ),
                      TextFormField(
                        controller: controller.addPassword,
                        decoration:
                            const InputDecoration(labelText: 'Password'),
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'Please enter a name';
                          }
                          return null;
                        },
                        onSaved: (value) {
                          _salesperson.name = value!;
                        },
                      ),
                      TextFormField(
                        decoration: const InputDecoration(labelText: 'Region'),
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'Please enter a region';
                          }
                          return null;
                        },
                        onSaved: (value) {
                          _salesperson.region = value!;
                        },
                      ),
                      ElevatedButton(
                        onPressed: () async {
                          controller.update();
                          var res = await controller.createUser(
                            controller.addUsername.text,
                            controller.addPassword.text,
                            controller.addJobNumber.text,
                            controller.image!,
                            controller.addRegion.text,
                          );
                          Get.snackbar("", res.toString());
                          Get.back();
                        },
                        child: const Text('Save'),
                      ),
                    ],
                  )
                : Center(child: const CircularProgressIndicator());
          }),
        ),
      ),
    );
  }

  void pickImage() async {
    final picker = ImagePicker();
    final pickedFile = await picker.pickImage(source: ImageSource.gallery);
    if (pickedFile != null) {
      controller.image = File(pickedFile.path);
      controller.update();
    } else {}
  }
}
