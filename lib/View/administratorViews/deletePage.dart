// ignore_for_file: file_names

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:salesperson/Controller/user_controller.dart';

class DeletePage extends GetView<UserController> {
  @override
  final controller = Get.put(UserController());

  DeletePage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Delete'),
      ),
      body: GetBuilder<UserController>(initState: (state) async {
        controller.allUsers = await controller.getAllUsersFromAPI();
        controller.update();
        print("Done");
      }, builder: (_) {
        return controller.isLoading.isFalse
            ? ListView.builder(
                itemCount: controller
                    .allUsers.length, //controller.salespersons.length,
                itemBuilder: (context, index) {
                  final salesperson = controller
                      .allUsers[index]; //controller.salespersons[index];
                  return Card(
                    child: ListTile(
                      leading: CircleAvatar(
                        backgroundImage: salesperson.image != null &&
                                salesperson.image != "" &&
                                salesperson.image!.isNotEmpty
                            ? AssetImage(salesperson.image.toString())
                            : const AssetImage(
                                "assets/images/unknownPerson.jpg"),
                      ),
                      title: Text(salesperson.userName.toString()),
                      subtitle: Text(salesperson.region.toString()),
                      trailing: IconButton(
                        icon: const Icon(Icons.delete),
                        onPressed: () {
                          // Delete salesperson
                          Get.defaultDialog(
                            title: 'Delete Salesperson',
                            content: Text(
                                'Are you sure you want to delete ${salesperson.userName}?'),
                            confirm: ElevatedButton(
                              onPressed: () async {
                                var res = await controller.deleteSalesperson(
                                    salesperson.id.toString());
                                controller.allUsers.removeAt(index);
                                Get.snackbar("", res);
                                Get.back();
                              },
                              child: const Text('Delete'),
                            ),
                            cancel: ElevatedButton(
                              onPressed: () {
                                Get.back();
                              },
                              child: const Text('Cancel'),
                            ),
                          );
                        },
                      ),
                    ),
                  );
                },
              )
            : const Center(child: CircularProgressIndicator());
      }),
    );
  }
}
