// ignore_for_file: file_names, depend_on_referenced_packages, avoid_print
/*
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:salesperson/View/Widgets/app_images.dart';

class EditPage extends StatelessWidget {
  const EditPage({super.key});

  @override
  Widget build(BuildContext context) {
    // Salesperson salesperson = SalespersonController.selectedSalesperson.value;

    return Scaffold(
      appBar: AppBar(
        title: const Text('Edit Salesperson'),
      ),
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage(AppPics.getRandomColor()),
              fit: BoxFit.cover,
              opacity: 0.2),
        ),
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              TextFormField(
                initialValue: "Hi", //salesperson.name,
                onChanged: (value) {
                  // SalespersonController.updateSalespersonName();
                },
                decoration: const InputDecoration(
                  labelText: 'Name',
                ),
              ),
              const SizedBox(height: 16.0),
              TextFormField(
                // initialValue: salesperson.region,
                onChanged: (value) {
                  print("Hello");
                  //SalespersonController.updateSalespersonRegion(value);
                },
                decoration: const InputDecoration(
                  labelText: 'Region',
                ),
              ),
              const SizedBox(height: 32.0),
              ElevatedButton(
                onPressed: () {
                  // SalespersonController.updateSalesperson();
                  Get.back();
                },
                child: const Text('Save'),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
*/

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:salesperson/Controller/user_controller.dart';
import 'package:salesperson/Model/user_model.dart';
import 'package:salesperson/View/Widgets/app_images.dart';

class EditPage extends GetView<UserController> {
  @override
  final controller = Get.put(UserController());
  EditPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Edit Sale Person'),
      ),
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage(AppPics.getRandomColor()),
              fit: BoxFit.cover,
              opacity: 0.2),
        ),
        child: GetBuilder<UserController>(initState: (state) {
          controller.getAllUsersFromAPI();
        }, builder: (_) {
          return controller.allUsers.isNotEmpty
              ? ListView.builder(
                  itemCount: controller.allUsers.length,
                  itemBuilder: (context, index) {
                    UserModel salesperson = controller.allUsers[index];
                    return ListTile(
                      title: Text(salesperson.userName.toString()),
                      subtitle: Text(salesperson.region.toString()),
                      trailing: IconButton(
                        icon: const Icon(Icons.edit),
                        onPressed: () {
                          Get.defaultDialog(
                            title: 'Update Salesperson',
                            content: Column(
                              children: [
                                TextFormField(
                                  initialValue: salesperson.userName,
                                  onChanged: (value) {
                                    salesperson.userName = value;
                                  },
                                  decoration: const InputDecoration(
                                    labelText: 'Name',
                                  ),
                                ),
                                TextFormField(
                                  initialValue: salesperson.password,
                                  onChanged: (value) {
                                    salesperson.password = value;
                                  },
                                  decoration: const InputDecoration(
                                    labelText: 'Password',
                                  ),
                                ),
                                TextFormField(
                                  initialValue: salesperson.region,
                                  onChanged: (value) {
                                    salesperson.region = value;
                                  },
                                  decoration: const InputDecoration(
                                    labelText: 'Region',
                                  ),
                                ),
                                TextFormField(
                                  initialValue: salesperson.isStaff,
                                  onChanged: (value) {
                                    salesperson.isStaff = value;
                                  },
                                  decoration: const InputDecoration(
                                    labelText: 'isStaff',
                                  ),
                                ),
                              ],
                            ),
                            confirm: ElevatedButton(
                              onPressed: () async {
                                var res = await controller.updateUser(
                                  id: salesperson.id ??
                                      controller.generateId().toString(),
                                  image: salesperson.image,
                                  jobNumber: salesperson.jobNumber,
                                  region: salesperson.region,
                                  username: salesperson.userName,
                                );
                                Get.back();
                                Get.snackbar("Update", res.toString());
                                controller.update();
                              },
                              child: const Text('Update'),
                            ),
                            cancel: ElevatedButton(
                              onPressed: () {
                                Get.back();
                              },
                              child: const Text('Cancel'),
                            ),
                          );
                        },
                      ),
                      onTap: () {
                        Get.defaultDialog(
                          title: 'Update Salesperson',
                          content: Column(
                            children: [
                              TextFormField(
                                initialValue: salesperson.userName,
                                onChanged: (value) {
                                  salesperson.userName = value;
                                },
                                decoration: const InputDecoration(
                                  labelText: 'Name',
                                ),
                              ),
                              TextFormField(
                                initialValue: salesperson.password,
                                onChanged: (value) {
                                  salesperson.password = value;
                                },
                                decoration: const InputDecoration(
                                  labelText: 'Password',
                                ),
                              ),
                              TextFormField(
                                initialValue: salesperson.region,
                                onChanged: (value) {
                                  salesperson.region = value;
                                },
                                decoration: const InputDecoration(
                                  labelText: 'Region',
                                ),
                              ),
                              TextFormField(
                                initialValue: salesperson.isStaff,
                                onChanged: (value) {
                                  salesperson.isStaff = value;
                                },
                                decoration: const InputDecoration(
                                  labelText: 'isStaff',
                                ),
                              ),
                            ],
                          ),
                          confirm: ElevatedButton(
                            onPressed: () async {
                              var res = await controller.updateUser(
                                id: salesperson.id ??
                                    controller.generateId().toString(),
                                image: salesperson.image,
                                jobNumber: salesperson.jobNumber,
                                region: salesperson.region,
                                username: salesperson.userName,
                              );
                              Get.back();
                              Get.snackbar("Update", res.toString());
                              controller.update();
                            },
                            child: const Text('Update'),
                          ),
                          cancel: ElevatedButton(
                            onPressed: () {
                              Get.back();
                            },
                            child: const Text('Cancel'),
                          ),
                        );
                      },
                    );
                  },
                )
              : Center(child: Image.asset("assets/images/noData.png"));
        }),
      ),
      /* floatingActionButton: FloatingActionButton(
        onPressed: () {
          // Add new salesperson
          Get.defaultDialog(
            title: 'Add Salesperson',
            content: Column(
              children: [
                TextFormField(
                  onChanged: (value) {
                    // Handle new salesperson name
                  },
                  decoration: const InputDecoration(
                    labelText: 'Name',
                  ),
                ),
                TextFormField(
                  onChanged: (value) {
                    // Handle new salesperson region
                  },
                  decoration: const InputDecoration(
                    labelText: 'Region',
                  ),
                ),
              ],
            ),
            confirm: ElevatedButton(
              onPressed: () {
                // Handle adding new salesperson to the list
                Get.back();
              },
              child: const Text('Add'),
            ),
            cancel: ElevatedButton(
              onPressed: () {
                Get.back();
              },
              child: const Text('Cancel'),
            ),
          );
        },
        child: const Icon(Icons.add),
      ),
    */
    );
  }
}




// import 'package:flutter/material.dart';
// import 'package:salesperson/View/Widgets/app_images.dart';

// class SearchPage extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: const Text('Search'),
//       ),
//       body: Container(
//         decoration: BoxDecoration(
//           image: DecorationImage(
//               image: AssetImage(AppPics.getRandomColor()),
//               fit: BoxFit.cover,
//               opacity: 0.2),
//         ),
//         child: const Center(
//           child: Text('Search Page'),
//         ),
//       ),
//     );
//   }
// }
