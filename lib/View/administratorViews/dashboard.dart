// ignore_for_file: prefer_const_constructors, depend_on_referenced_packages

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:salesperson/View/Widgets/app_colors.dart';
import 'package:salesperson/View/Widgets/app_images.dart';
import 'package:salesperson/View/administratorViews/AddPage.dart';
import 'package:salesperson/View/administratorViews/EditPage.dart';
import 'package:salesperson/View/administratorViews/SearchPage.dart';
import 'package:salesperson/View/administratorViews/StatisticsPage.dart';
import 'package:salesperson/View/administratorViews/deletePage.dart';

class DashBoard extends StatelessWidget {
  const DashBoard({super.key});

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: SingleChildScrollView(
          child: Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage(AppPics.getRandomColor()),
                  fit: BoxFit.cover,
                  opacity: 0.2),
            ),
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.all(12.0),
                  child: Row(
                    children: [
                      Text(
                        'Dashboard',
                        style: TextStyle(
                            fontSize: 30.sp,
                            fontWeight: FontWeight.bold,
                            color: AppColors.mainColor),
                      ),
                      const Spacer(),
                    ],
                  ),
                ),
                SizedBox(
                  width: Get.width,
                  height: Get.height,
                  child: GridView.count(
                    crossAxisCount: 2,
                    padding: const EdgeInsets.all(16.0),
                    children: [
                      _buildGridItem('Edit', Icons.edit, EditPage()),
                      _buildGridItem('Add', Icons.add, AddPage()),
                      _buildGridItem('Delete', Icons.delete, DeletePage()),
                      _buildGridItem(
                          'Statistics', Icons.bar_chart, StatisticsPage()),
                      _buildGridItem('Search', Icons.search, SearchPage()),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildGridItem(String title, IconData icon, Widget page) {
    return GestureDetector(
      onTap: () {
        // Get.to(DashboardView());
        Get.to(page);
      },
      child: Card(
        color: AppColors.getRandomColor(),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Icon(
              icon,
              size: 45.h,
              color: AppColors.whiteColor,
            ),
            SizedBox(height: 8.h),
            Text(title,
                style: TextStyle(fontSize: 16.0, color: AppColors.whiteColor)),
          ],
        ),
      ),
    );
  }
}
