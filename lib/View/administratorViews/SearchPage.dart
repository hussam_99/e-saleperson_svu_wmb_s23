// ignore_for_file: unused_local_variable, file_names

import 'package:flutter/material.dart';
import 'package:get/get.dart';

class SearchPage extends StatelessWidget {
  final CommissionController _commissionController =
      Get.put(CommissionController());

   SearchPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Commission Search'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          children: [
            DropdownButtonFormField<int>(
              value: _commissionController.selectedMonth.value,
              items: List.generate(12, (index) {
                return DropdownMenuItem<int>(
                  value: index + 1,
                  child: Text('${index + 1}'),
                );
              }),
              onChanged: (value) {
                _commissionController.selectedMonth.value = value!;
              },
              decoration: const InputDecoration(labelText: 'Month'),
            ),
            DropdownButtonFormField<int>(
              value: _commissionController.selectedYear.value,
              items: List.generate(10, (index) {
                int year = DateTime.now().year - index;
                return DropdownMenuItem<int>(
                  value: year,
                  child: Text('$year'),
                );
              }),
              onChanged: (value) {
                _commissionController.selectedYear.value = value!;
              },
              decoration: const InputDecoration(labelText: 'Year'),
            ),
            ElevatedButton(
              onPressed: () => _commissionController.searchCommissions(),
              child: const Text('Search'),
            ),
            const SizedBox(height: 16.0),
            Expanded(
              child: Obx(() {
                if (_commissionController.isLoading.value) {
                  return const Center(child: CircularProgressIndicator());
                } else if (_commissionController.commissions.isEmpty) {
                  return const Center(child: Text('No commissions found'));
                } else {
                  return ListView.builder(
                    itemCount: _commissionController.commissions.length,
                    itemBuilder: (context, index) {
                      Commission commission =
                          _commissionController.commissions[index];
                      return ListTile(
                        title: Text(commission.name),
                        subtitle: Text(commission.region),
                        trailing:
                            Text('\$${commission.amount.toStringAsFixed(2)}'),
                      );
                    },
                  );
                }
              }),
            ),
          ],
        ),
      ),
    );
  }
}

class Commission {
  final String name;
  final String region;
  final double amount;

  Commission({required this.name, required this.region, required this.amount});
}

class CommissionController extends GetxController {
  final RxInt selectedMonth = 1.obs;
  final RxInt selectedYear = DateTime.now().year.obs;
  final RxList<Commission> commissions = <Commission>[].obs;
  final RxBool isLoading = false.obs;

  void searchCommissions() {
    int month = selectedMonth.value;
    int year = selectedYear.value;

    // Perform search logic here
    // Replace with your own implementation to fetch search results from the backend

    // Dummy search results for demonstration purposes
    List<Commission> dummyResults = [
      Commission(name: 'John Doe', region: 'North', amount: 5000),
      Commission(name: 'Jane Smith', region: 'South', amount: 7500),
      Commission(name: 'Mike Johnson', region: 'West', amount: 3000),
    ];

    isLoading.value = true;

    // Simulate API call delay
    Future.delayed(const Duration(seconds: 2), () {
      commissions.value = dummyResults;
      isLoading.value = false;
    });
  }
}



/*

// ignore_for_file: use_key_in_widget_constructors, depend_on_referenced_packages, file_names

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:salesperson/Controller/admin_controller.dart';
import 'package:salesperson/Model/sales_person_model.dart';
import 'package:salesperson/View/Widgets/app_images.dart';

class SearchPage extends GetView<AdminController> {
  @override
  final controller = Get.put(AdminController());
  SearchPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Dashboard'),
      ),
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage(AppPics.getRandomColor()),
              fit: BoxFit.cover,
              opacity: 0.2),
        ),
        child: GetBuilder<AdminController>(builder: (_) {
          return controller.salespersons.isNotEmpty
              ? ListView.builder(
                  itemCount: controller.salespersons.length,
                  itemBuilder: (context, index) {
                    Salesperson salesperson = controller.salespersons[index];
                    return ListTile(
                      title: Text(salesperson.name.toString()),
                      subtitle: Text(salesperson.region.toString()),
                      trailing: IconButton(
                        icon: const Icon(Icons.delete),
                        onPressed: () {
                          // Delete salesperson
                          Get.defaultDialog(
                            title: 'Delete Salesperson',
                            content: Text(
                                'Are you sure you want to delete ${salesperson.name}?'),
                            confirm: ElevatedButton(
                              onPressed: () {
                                controller.salespersons.removeAt(index);
                                controller.update();
                                Get.back();
                              },
                              child: const Text('Delete'),
                            ),
                            cancel: ElevatedButton(
                              onPressed: () {
                                Get.back();
                              },
                              child: const Text('Cancel'),
                            ),
                          );
                        },
                      ),
                      onTap: () {
                        // Update salesperson information
                        Get.defaultDialog(
                          title: 'Update Salesperson',
                          content: Column(
                            children: [
                              TextFormField(
                                initialValue: salesperson.name,
                                onChanged: (value) {
                                  salesperson.name = value;
                                },
                                decoration: const InputDecoration(
                                  labelText: 'Name',
                                ),
                              ),
                              TextFormField(
                                initialValue: salesperson.region,
                                onChanged: (value) {
                                  salesperson.region = value;
                                },
                                decoration: const InputDecoration(
                                  labelText: 'Region',
                                ),
                              ),
                            ],
                          ),
                          confirm: ElevatedButton(
                            onPressed: () {
                              Get.back();
                            },
                            child: const Text('Update'),
                          ),
                          cancel: ElevatedButton(
                            onPressed: () {
                              Get.back();
                            },
                            child: const Text('Cancel'),
                          ),
                        );
                      },
                    );
                  },
                )
              : Center(child: Image.asset("assets/images/noData.png"));
        }),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          // Add new salesperson
          Get.defaultDialog(
            title: 'Add Salesperson',
            content: Column(
              children: [
                TextFormField(
                  onChanged: (value) {
                    // Handle new salesperson name
                  },
                  decoration: const InputDecoration(
                    labelText: 'Name',
                  ),
                ),
                TextFormField(
                  onChanged: (value) {
                    // Handle new salesperson region
                  },
                  decoration: const InputDecoration(
                    labelText: 'Region',
                  ),
                ),
              ],
            ),
            confirm: ElevatedButton(
              onPressed: () {
                // Handle adding new salesperson to the list
                Get.back();
              },
              child: const Text('Add'),
            ),
            cancel: ElevatedButton(
              onPressed: () {
                Get.back();
              },
              child: const Text('Cancel'),
            ),
          );
        },
        child: const Icon(Icons.add),
      ),
    );
  }
}




// import 'package:flutter/material.dart';
// import 'package:salesperson/View/Widgets/app_images.dart';

// class SearchPage extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: const Text('Search'),
//       ),
//       body: Container(
//         decoration: BoxDecoration(
//           image: DecorationImage(
//               image: AssetImage(AppPics.getRandomColor()),
//               fit: BoxFit.cover,
//               opacity: 0.2),
//         ),
//         child: const Center(
//           child: Text('Search Page'),
//         ),
//       ),
//     );
//   }
// }
*/