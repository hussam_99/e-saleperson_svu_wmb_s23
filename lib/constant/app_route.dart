class AppRoute {
  static const String splash = "/";
  static const String login = "/login";
  static const String second = "/second";
  static const String third = "/third";
  static const String sigup = "/sigup";
  static const String calc = "/calc";
  static const String enterNewEmp = "/enterNewEmp";
  static const String home = "/home";
  static const String lottie1 = "/page1";
  static const String lottie2 = "/page2";
  static const String lottie3 = "/page3";
  static const String dashboard = "/dashboard";

  // static const String enterNewEmp = "/enterNewEmp";
  // static const String enterNewEmp = "/enterNewEmp";
  // static const String enterNewEmp = "/enterNewEmp";
  // static const String enterNewEmp = "/enterNewEmp";
}
