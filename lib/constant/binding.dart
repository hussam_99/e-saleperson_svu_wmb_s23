import 'package:get/get.dart';
import 'package:salesperson/Controller/user_controller.dart';

class SampleBind extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<UserController>(() => UserController());
    // Get.lazyPut<HomeController>(() => HomeController());
  }
}
