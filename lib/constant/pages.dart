// ignore_for_file: prefer_const_constructors, depend_on_referenced_packages

import 'package:get/get.dart';
import 'package:salesperson/View/RegularPersonViews/commission_page.dart';
import 'package:salesperson/View/administratorViews/dashboard.dart';
import 'package:salesperson/View/pages/homePage.dart';
import 'package:salesperson/View/pages/log_in.dart';
import 'package:salesperson/View/pages/sign_up.dart';
import 'package:salesperson/View/pages/splash_screen.dart';
import 'package:salesperson/constant/app_route.dart';
import 'package:salesperson/constant/transitions.dart';

import '../View/pages/lottie_splash_screen.dart';

List<GetPage> listOfPages = [
  GetPage(
    name: AppRoute.splash,
    page: () =>  SplashScreen(),
  ),
  GetPage(
    name: AppRoute.login,
    page: () => LoginPage(),
  ),
  GetPage(
    name: '/sigup',
    customTransition: SizeTransitions(),
    page: () => SignUpView(),
  ),
  GetPage(
    name: '/calc',
    customTransition: SizeTransitions(),
    page: () => CommissionPage(),
  ),
  GetPage(
    name: '/home',
    customTransition: SizeTransitions(),
    page: () => HomePage(),
  ),
  GetPage(name: '/page1', page: () => Page1()),
  GetPage(name: '/page2', page: () => Page2()),
  GetPage(name: '/page3', page: () => Page3()),
  GetPage(name: "/dashboard", page: () => DashBoard()),
];
