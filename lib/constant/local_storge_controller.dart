import 'package:get/get.dart';
import 'package:hive_flutter/hive_flutter.dart';


class LocalStorage extends GetxController {
  Box box = Hive.box("idbook");
  Box profile = Hive.box("profile");
  Box showcase = Hive.box("showCase");
  // Box jobSyncDate = Hive.box("job_sync_date");
  // Box placeFavorites = Hive.box<PlaceFavorite>("place_favorite");

  // Box productFavorite = Hive.box<ProductFavorite>("product_favorite");
  // Box qrHistory = Hive.box<QrHistoryModel>("qrHistory");
  // Box savedLocations = Hive.box<SavedLocation>("savedLocations");
  // Box customFilters = Hive.box<CustomFilter>("customFilters");
  // Box notifications = Hive.box<Notification>("notifications");
  // Box notificationsDraft = Hive.box<Notification>("notifications-draft");
  // Box notificationsApp = Hive.box<Notification>("notifications-app");
  // Box userJobs = Hive.box<UserJobs>("userJobs");
  // Box userSpecialities = Hive.box<UserSpecialityModel>("userSpecialities");
  // Box mainCategories = Hive.box<MainCategory>("mainCategories");
  // Box secondCategories = Hive.box<SecondCategory>("secondCategories");

  @override
  void onClose() {
    super.onClose();
    box.close();
    profile.close();
    // jobSyncDate.close();
    // placeFavorites.close();
    // productFavorite.close();
    // qrHistory.close();
    // savedLocations.close();
    // customFilters.close();
    // notifications.close();
    // notificationsDraft.close();
    // notificationsApp.close();
    // userJobs.close();
    // userSpecialities.close();
    // mainCategories.close();
    // secondCategories.close();
  }
}
