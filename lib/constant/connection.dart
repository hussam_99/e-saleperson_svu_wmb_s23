import 'dart:async';
import 'dart:convert';
import 'dart:developer';
import 'dart:io';
import 'package:get/get.dart';
// ignore: depend_on_referenced_packages
import 'package:http/http.dart' as http;
import 'package:salesperson/constant/local_storge_controller.dart';

String date = DateTime.now().toString().substring(0, 10);
DateTime timeStamp() {
  return DateTime.parse(DateTime.now().toString().substring(0, 19));
}

checkInternet() async {
  try {
    var result = await InternetAddress.lookup("google.com");
    if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
      return true;
    }
  } on SocketException catch (_) {
    return false;
  }
}

var client = http.Client();
// /*GetSnackBar snakonline = GetSnackBar(
//   message: "connecting".tr,
//   backgroundGradient: Colors.red,
//   duration: const Duration(days: 365),
//   snackPosition: SnackPosition.BOTTOM,
//   snackStyle: SnackStyle.FLOATING,
//   margin: const EdgeInsets.only(bottom: 20, left: 20, right: 20, top: 10),
//   borderRadius: 12,
//   forwardAnimationCurve: Curves.slowMiddle,
//   mainButton: IconButton(
//       splashColor: Colors.transparent,
//       onPressed: () => Get.closeCurrentSnackbar(),
//       icon: const Icon(Icons.close)),
// );
// */
// connectGet(String linkurl, Map<String, String>? header) async {
//   if (await checkInternet()) {
//     var response = await http.get(
//       Uri.parse(linkurl),
//       headers: header,
//     );
//     if (response.statusCode == 200 || response.statusCode == 201) {
//       List responsebody = jsonDecode(response.body);
//       return responsebody;
//     } else if (401 == response.statusCode) {
//       if (LocalStorage().box.get("refresh_token") != null) {
//         Map res = refreshToken();
//         if (res.isNotEmpty) {
//           return StatusRequest.failure;
//         }
//         return await connectGet(linkurl, header);
//       }
//      // UserController.logout();
//       return StatusRequest.serverfailure;
//     } else {
//       return StatusRequest.serverfailure;
//     }
//   } else {
//     return StatusRequest.offlinefailure;
//   }
// }

// // const String baseUrl = "https://idbook.yesforshopping.com/v1_basic";
// // http://185.194.124.120
// // http://192.168.1.102/yes/idbook/
const String baseDomain = "https://idbook.yesforshopping.com/";
const String apiPerfix = "v1_basic";
const String baseUrl = "$baseDomain/$apiPerfix";
Future connect(Map arag, String fileName, {bool multi = false}) async {
  log("connect");
  var error = <String, String>{};
  bool check = await checkInternet();

  if (check) {
  } else {
    error["error"] = "no internet";
    return error;
  }
  client = http.Client();
//   // if (needRefresh && fileName != "user/signup" && fileName != "user/active") {
//   //   Map res = await refreshToken();
//   //   if (res.isNotEmpty) {
//   //     return res;
//   //   }
//   //  needRefresh = false;
//   }
  var root = Uri.parse('$baseUrl/$fileName.php');
  var header = <String, String>{};
  String? jwt = LocalStorage().box.get("jwt");
  if (jwt != null) {
    header[HttpHeaders.authorizationHeader] = 'Bearer $jwt';
  }
  header[HttpHeaders.accessControlAllowOriginHeader] = '*';
  header[HttpHeaders.accessControlAllowMethodsHeader] = 'POST, GET';
  header[HttpHeaders.acceptHeader] = 'application/json';
  try {
    dynamic uid = LocalStorage().box.get("id");
    if (uid != null) {
      arag["uid"] = uid.toString();
    }
    log("=============== arag arag =====================");
    log("arag = $arag");
    final response = await client.post(root, body: arag, headers: header);
    log("=============== arag arag =====================");
    log("arag = $arag");
    log("====================================");
    log("status Code is = ${response.statusCode} **  Response body is = ${response.body}");
    log("====================================");

    if (200 == response.statusCode) {
      LocalStorage().box.put("jwt", jsonDecode(response.body)["jwt"]);
      return jsonDecode(response.body);
    }
    if (401 == response.statusCode || 400 == response.statusCode) {
      if (LocalStorage().box.get("refresh_token") != null) {
        //Map res =;//await refreshToken();
        String res = "";
        if (res.isNotEmpty) {
          return res;
        }
        return await connect(arag, fileName);
      }
//       UserController.logout();
      return {"error": "Access denied".tr};
    }
    if (500 == response.statusCode) {
      // showMessage(
      //     "خطأ في السيرفر، رقم الخطأ ${jsonDecode(response.body)["error_id"]}",
      //     AppColors.error);
      return {"error": "Server Error"};
    }
  } on SocketException {
    error["error"] = "no internet";
  } on HttpException {
    error["error"] = "There is an error in the server".tr;
  } on FormatException {
    error["error"] = "There is an error in demand".tr;
  } on TimeoutException {
    error["error"] = "Demand has ended".tr;
  } catch (e) {
    log("connect func =$e");
    error["error"] = "There is a problem connection, try again".tr;
  }
  return error;
}

// Future<Map> connectWithFiles(Map<String, String> args, String fileName,
//     List<String> files, String fieldName,
//     {bool multi = false}) async {
//   var error = <String, String>{};
//   String? jwt = LocalStorage().box.get("jwt");
//   if (needRefresh && fileName != "user/signup" && fileName != "user/active") {
//     Map res = await refreshToken();
//     if (res.isNotEmpty) {
//       return res;
//     }
//     needRefresh = false;
//   }
//   try {
//     var request =
//         http.MultipartRequest('POST', Uri.parse('$baseUrl/$fileName.php'));
//     request.headers[HttpHeaders.authorizationHeader] = "Bearer $jwt";
//     request.headers[HttpHeaders.acceptHeader] = '*/*';
//     request.headers[HttpHeaders.accessControlAllowOriginHeader] = '*';
//     request.headers[HttpHeaders.accessControlAllowMethodsHeader] = 'POST, GET';
//     dynamic uid = LocalStorage().box.get("id");
//     if (uid != null) {
//       args["uid"] = uid.toString();
//     }
//     request.fields.addAll(args);
//     if (files.isNotEmpty) {
//       if (multi) {
//         int i = 0;
//         for (String f in files) {
//           request.files
//               .add(await http.MultipartFile.fromPath("$fieldName[$i]", f));
//           i++;
//         }
//       } else {
//         request.files
//             .add(await http.MultipartFile.fromPath(fieldName, files[0]));
//       }
//     }
//     var response = await request.send();
//     final resBody = await response.stream.bytesToString();
//     log("=============== arag arag =====================");
//     log("arag = $args");
//     log("====================================");
//     log("status Code is = ${response.statusCode} **  Response body is = $resBody");
//     log("====================================");
//     if (response.statusCode >= 200 && response.statusCode < 300) {
//       LocalStorage().box.put("jwt", jsonDecode(resBody)["jwt"]);
//       return jsonDecode(resBody);
//     } else if (401 == response.statusCode) {
//       if (LocalStorage().box.get("refresh_token") != null) {
//         Map res = await refreshToken();
//         if (res.isNotEmpty) {
//           return res;
//         }
//         return await connectWithFiles(args, fileName, files, fieldName,
//             multi: multi);
//       }
//       UserController.logout();
//       return {"error": "Access denied".tr};
//     } else if (500 == response.statusCode) {
//       showMessage(
//           "خطأ في السيرفر، رقم الخطأ: ${jsonDecode(resBody)["error_id"]}",
//           AppColors.error);
//       return {"error": "Server Error"};
//     } else {
//       error["error"] = response.reasonPhrase ?? 'unkown error';
//     }
//   } on SocketException {
//     error["error"] = "Make sure your internet connection".tr;
//   } on HttpException {
//     error["error"] = "There is an error in the server".tr;
//   } on FormatException {
//     error["error"] = "There is an error in demand".tr;
//   } on TimeoutException {
//     error["error"] = "Demand has ended".tr;
//   } catch (e) {
//     log("connect wit files func =$e");
//     error["error"] = "There is a problem connection, try again".tr;
//   }
//   return error;
// }

// refreshToken() async {
//   var auth = Uri.parse('$baseUrl/auth/auth');
//   var authheader = <String, String>{};
//   Map error = {};
//   authheader[HttpHeaders.accessControlAllowOriginHeader] = '*';
//   authheader[HttpHeaders.accessControlAllowMethodsHeader] =
//       'POST, GET, OPTIONS, PUT, DELETE, HEAD';
//   authheader[HttpHeaders.acceptHeader] = 'application/json';
//   try {
//     Map args = {
//       "action": "auth",
//       "uid": LocalStorage().box.get("id").toString(),
//       "refresh_token": LocalStorage().box.get("refresh_token"),
//     };
//     final response = await client.post(auth, body: args, headers: authheader);
//     log("******* ${response.statusCode} **** res body ${response.body}");
//     if (200 == response.statusCode) {
//       LocalStorage().box.put("jwt", jsonDecode(response.body)["jwt"]);
//       LocalStorage()
//           .box
//           .put("refresh_token", jsonDecode(response.body)["refresh_token"]);
//       needRefresh = false;
//       timer = Timer(
//         const Duration(minutes: 10),
//         () => needRefresh = true,
//       );
//       return {};
//     } else if (401 == response.statusCode) {
//       UserController.logout();
//       return {};
//     }
//   } on SocketException {
//     error["error"] = "Make sure your internet connection".tr;
//   } on HttpException {
//     error["error"] = "There is an error in the server".tr;
//   } on FormatException {
//     error["error"] = "There is an error in demand".tr;
//   } on TimeoutException {
//     error["error"] = "Demand has ended".tr;
//   } catch (e) {
//     log("refresh token func =$e");
//     error["error"] = "There is a problem connection, try again".tr;
//   }
//   return error;
// }

// bool reg(String compare, String reg) {
//   RegExp regExp = RegExp(reg);
//   if (regExp.hasMatch(compare) && compare.isNotEmpty) {
//     return true;
//   } else {
//     return false;
//   }
// }

// showMessage(String message, Color bgColor,
//     {Duration duration = const Duration(seconds: 3), Color? textcolor}) {
//   Get.isSnackbarOpen ? Get.closeCurrentSnackbar() : null;
//   Get.rawSnackbar(
//     messageText: Text(
//       message,
//       style: AppStyles.primaryTextStyle()
//           .copyWith(color: textcolor ?? AppColors.whiteColor),
//     ),
//     backgroundColor: bgColor,
//     snackPosition: SnackPosition.BOTTOM,
//     snackStyle: SnackStyle.FLOATING,
//     margin: const EdgeInsets.only(bottom: 20, left: 20, right: 20, top: 10),
//     borderRadius: 12,
//     forwardAnimationCurve: Curves.easeOutSine,
//   );
// }

// showStickeyMessage(String message, Color bgColor) {
//   Get.isSnackbarOpen ? Get.closeCurrentSnackbar() : null;
//   Get.rawSnackbar(
//     messageText: Text(
//       message,
//       style: AppStyles.primaryTextStyle().copyWith(color: AppColors.whiteColor),
//     ),
//     backgroundColor: bgColor,
//     duration: const Duration(minutes: 30),
//     dismissDirection: DismissDirection.horizontal,
//     mainButton: IconButton(
//         splashColor: Colors.transparent,
//         onPressed: () => Get.closeCurrentSnackbar(),
//         icon: const Icon(Icons.close)),
//     snackPosition: SnackPosition.BOTTOM,
//     snackStyle: SnackStyle.FLOATING,
//     margin: const EdgeInsets.only(bottom: 20, left: 20, right: 20, top: 10),
//     borderRadius: 12,
//     forwardAnimationCurve: Curves.easeOutSine,
//   );
// }

// String myDateTimeFormat(DateTime dateTime) {
//   log(Get.deviceLocale.toString());
//   return intl.DateFormat.yMd(Get.deviceLocale!.languageCode)
//       .add_jm()
//       .format(dateTime);
// }

// Future<void> callPhone(String phoneNumber) async {
//   final Uri launchUri = Uri(
//     scheme: 'tel',
//     path: phoneNumber,
//   );
//   await launchUrl(launchUri);
// }

// bool isRTL(String text) {
//   return intl.Bidi.detectRtlDirectionality(text);
// }

// showNotificationSnack(String title, String body,
//     {Function? buttonHandler, String? buttonText}) {
//   Get.rawSnackbar(
//     titleText: Padding(
//       padding: EdgeInsets.only(top: 10.h),
//       child: Text(
//         title,
//         style: AppStyles.primaryTextStyle().copyWith(
//           fontSize: 28.sp,
//           color: AppColors.mainColor,
//         ),
//       ),
//     ),
//     messageText: Padding(
//       padding: EdgeInsets.symmetric(vertical: 10.h),
//       child: Text(
//         body,
//         style: AppStyles.primaryTextStyle().copyWith(
//           color: AppColors.secColor,
//           height: isRTL(body) ? 1.3 : 1,
//         ),
//       ),
//     ),
//     backgroundColor: AppColors.kBackgroundColor,
//     duration: const Duration(seconds: 6),
//     dismissDirection: DismissDirection.horizontal,
//     leftBarIndicatorColor: AppColors.buttonColor,
//     mainButton: Padding(
//       padding: EdgeInsets.symmetric(horizontal: 10.w),
//       child: ElevatedButton(
//           onPressed: () {
//             Get.closeCurrentSnackbar();
//             if (buttonHandler != null) {
//               buttonHandler();
//             }
//           },
//           child: Text(
//             buttonText ?? 'ok'.tr,
//             style: AppStyles.primaryTextStyle().copyWith(
//               color: AppColors.whiteColor,
//             ),
//           )),
//     ),
//     snackPosition: SnackPosition.TOP,
//     snackStyle: SnackStyle.FLOATING,
//     margin: EdgeInsets.symmetric(horizontal: 20.w, vertical: 20.h),
//     padding: EdgeInsets.zero,
//     forwardAnimationCurve: Curves.easeOutSine,
//   );
// }
