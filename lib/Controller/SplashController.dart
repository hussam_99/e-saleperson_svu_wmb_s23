import 'package:connectivity/connectivity.dart';
import 'package:get/get.dart';
import 'package:salesperson/Model/user_model.dart';
import 'package:salesperson/View/pages/noInternetConn.dart';
import 'package:salesperson/constant/app_route.dart';

class SplashController extends GetxController {
  double logoOpacity = 0.0;
  double textOpacity = 0.0;

  @override
  void onInit() {
    super.onInit();
    animateItems();
  }

  void animateItems() async {
    await Future.delayed(const Duration(milliseconds: 500));
    logoOpacity = 1.0;
    update();

    await Future.delayed(const Duration(milliseconds: 500));
    textOpacity = 1.0;
    update();

    await Future.delayed(const Duration(seconds: 2));
    bool isConnected = await checkInternetConnection();
//  checkInternetConnection()
    if (isConnected) {
      await UserModel.isUserStaff()
          ? Get.offNamed(AppRoute.dashboard)
          : Get.offNamed(AppRoute.home);
    } else {
      Get.to(const NoInternetConn());
    }

    // Get.offNamed(AppRoute.login); // Replace '/login' with your login page route
  }

  Future<bool> checkInternetConnection() async {
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.none) {
      return false; // No internet connection
    } else {
      return true; // Internet connection available
    }
  }
}
