import 'dart:convert';
import 'dart:io';
import 'dart:math';
import 'package:get/get_rx/src/rx_types/rx_types.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:get/get_state_manager/src/simple/get_controllers.dart';
import 'package:salesperson/Api/AppUrls.dart';
import 'package:salesperson/Model/sales_person_model.dart';
import 'package:salesperson/Model/user_model.dart';

class UserController extends GetxController {
  List<UserModel> allUsers = [];
  String selectedRegion = '';
  File? image;
  RxBool isLoading = false.obs;
  late UserModel currentUser;
  //############################ suer Info
  TextEditingController nameController = TextEditingController();
  TextEditingController regionController = TextEditingController();
  TextEditingController uniqueNumberController = TextEditingController();
//############################ LogIn
  TextEditingController logInUserNameController = TextEditingController();
  TextEditingController logInPasswordNameController = TextEditingController();
//############################ LogIn
  TextEditingController signUpUserNameController = TextEditingController();
  TextEditingController signUpPasswordNameController = TextEditingController();
  TextEditingController signUpConfirmPasswordNameController =
      TextEditingController();
//############################ add new user
  final TextEditingController addUsername = TextEditingController();
  final TextEditingController addPassword = TextEditingController();
  final TextEditingController addRegion = TextEditingController();
  final TextEditingController addJobNumber = TextEditingController();
  final List<String> regions = [
    'Lebanon Region',
    'Coastal Region',
    'Southern Region',
    'Northern Region',
    'Eastern Region',
  ];
//################################
  TextEditingController salespersonNumberCTRL = TextEditingController();
  TextEditingController salespersonNameCTRL = TextEditingController();
  TextEditingController monthCTRL = TextEditingController();
  TextEditingController registrationDateCTRL = TextEditingController();
  TextEditingController southernregionCTRL = TextEditingController();
  TextEditingController coastalregionCTRL = TextEditingController();
  TextEditingController northernRegionCTRL = TextEditingController();
  TextEditingController easternRegionCTRL = TextEditingController();
  TextEditingController lebanonCTRL = TextEditingController();
  TextEditingController monthlyCommissionCTRL = TextEditingController();
//################################

  final salespersons = <Salesperson>[
    Salesperson(
        id: 386, name: "Hussam", region: "Lebanon", photo: "assets/hussam.jpg"),
    Salesperson(id: 1, name: 'John Doe', region: 'Region A'),
    Salesperson(id: 2, name: 'Jane Smith', region: 'Region B'),
    Salesperson(id: 3, name: 'Mike Johnson', region: 'Region C'),
  ].obs;

  void addSalesperson(Salesperson salesperson) {
    salespersons.add(salesperson);
  }

  // void deleteSalespersonloaclly(int salespersonId) {
  //   salespersons.removeWhere((element) => element.id == salespersonId);
  // }

  @override
  Future<void> onInit() async {
    super.onInit();
  }

//Done
  Future<String> signIn() async {
    if (logInUserNameController.text.isNotEmpty &&
        logInPasswordNameController.text.isNotEmpty) {
      isLoading.value = true;
      update();
      var res = await UserModel.signIn(
          logInUserNameController.text, logInPasswordNameController.text);
      if (res == "error") {
        isLoading.value = false;
        update();
        return "error";
      } else {
        isLoading.value = false;
        update();
        // log("success");
        return "success";
      }
    } else {
      return "null";
    }
  }

  void showMessage(String message) {
    // Show the message using Get.snackbar or any other method
  }

  Future<String> updateUser(
      {String? id,
      String? username,
      String? jobNumber,
      String? image,
      String? region}) async {
    // Create the request body
    Map<String, dynamic> requestBody = {
      'username': username,
      'job_number': jobNumber,
      'image': image,
      'region': region,
    };
    isLoading.value = true;
    // Send the request to the API
    final response = await http.post(
      Uri.parse(AppURL.Edit),
      headers: {'Content-Type': 'application/json'},
      body: jsonEncode(requestBody),
    );
    print(response);
    if (response.statusCode == 201) {
      print('User created successfully!');
      isLoading.value = false;
      return 'User Updated successfully!';
    } else {
      isLoading.value = false;
      print("No");
      return 'Failed to Update user. Status code: ${response.statusCode}';
    }
  }

//Done
  Future<String> deleteSalesperson(String id) async {
    isLoading.value = true;
    var url =
        Uri.parse('http://haya244099.pythonanywhere.com/core/auth/delete/$id/');
    var response = await http.delete(url);

    if (response.statusCode == 200) {
      showMessage('Salesperson deleted successfully');
      isLoading.value = false;
      update();
      getAllUsersFromAPI();
      return 'Salesperson deleted successfully';
    } else {
      showMessage('Failed to delete salesperson');
      return 'Failed to delete salesperson';
    }
  }

  Future<List<UserModel>> getAllUsersFromAPI() async {
    isLoading.value = true;
    final response = await http.get(
        Uri.parse('https://haya244099.pythonanywhere.com/core/all-users/'));

    if (response.statusCode == 200) {
      final jsonData = json.decode(response.body);
      List<UserModel> users = [];

      for (var user in jsonData) {
        users.add(UserModel(
          id: user['id'].toString(),
          userName: user['userName'].toString(),
          password: user['password'].toString(),
          isStaff: user['isStaff'].toString(),
          jobNumber: user['jobNumber'].toString(),
          image: user['image'].toString(),
          region: user['region'].toString(),
        ));
      }
      print(users);
      isLoading.value = false;
      update();
      return users;
    } else {
      throw Exception('Failed to load users from API');
    }
  }

  Future<String> createUser(
    String username,
    String password,
    String jobNumber,
    File image,
    String region,
  ) async {
    // Create the request body
    Map<String, dynamic> requestBody = {
      'username': username,
      'password': password,
      'job_number': jobNumber,
      'image': image,
      'region': region,
    };
    isLoading.value = true;
    // Send the request to the API
    final response = await http.post(
      Uri.parse('https://haya244099.pythonanywhere.com/core/all-users/'),
      headers: {'Content-Type': 'application/json'},
      body: jsonEncode(requestBody),
    );
    print(response);
    if (response.statusCode == 201) {
      print('User created successfully!');
      isLoading.value = false;
      return 'User created successfully!';
    } else {
      isLoading.value = false;
      print('User created no!');

      return 'Failed to create user. Status code: ${response.statusCode}';
    }
  }

  int generateId() {
    Random random = Random();
    return random.nextInt(100) + 1;
  }
}
