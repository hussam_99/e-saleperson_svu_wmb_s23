// ignore_for_file: non_constant_identifier_names, file_names

class AppURL {
  static const String baseDomian = "https://haya244099.pythonanywhere.com";
  static String SignIn = "$baseDomian/auth/jwt/token/";
  static String SignUp = "$baseDomian/auth/users/";
  static String Edit = "$baseDomian/core/modify/";
  static String Delete = "$baseDomian/core/auth/delete/";
  static String UserInfo = "$baseDomian/core/user/detail/";
}
