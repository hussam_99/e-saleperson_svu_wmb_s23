
note:
*********
 the admin is: username: admin , password: admin and have id :1 

******************************************************************
2.	Create first route for username and password.
login with Post requset 
   https://haya244099.pythonanywhere.com/core/auth/jwt/token/
json
{
    "username": "",
    "password": ""
}
*********************************************************************

//For Administrator
4.Design third route for entering the numbers, names, photos and main regions of the Salespersons, and save it in a suitable database, This route allows deleting and modifying the Salesperson information (only administrator).

Register with Post requset 
  https://haya244099.pythonanywhere.com/auth/users/ 
json 
{
    "username": "",
    "password": "",
    "job_number": "",
    "image": "",
    "region": ""
}
modifying  with PATCH requset
   https://haya244099.pythonanywhere.com/core/modify/2/
json
{
    "id": "",
    "username": "",
    "job_number": "",
    "image": "",
    "region":""
}
deleting 
    with delete request according id for salespersone
    http://haya244099.pythonanywhere.com/core/auth/delete/3/

****************************************************




5.	Design forth route that allows:
a.	Selecting the Salesperson’s name, the Salesperson’s region is displayed in read only field, and displays his photo as well.

 with Get requset note: the 4 is te id for selesperson

      https://haya244099.pythonanywhere.com/core/user/detail/4/

b.	Entering the selling amount in every region

with Post requset 

     http://127.0.0.1:8000/core/api/sales/

json 
{
    
    "date": "2023-10-25",
    "sales_amount": 900000.0,
    "score": 20.0,
    "salesperson": 2,
    "region": 2
}


c.	Calculate the Monthly commission for every salesperson according to every region as shown in the above example and store the selling and commission information.

        http://haya244099.pythonanywhere.com/core/calculate-monthly-score/<int:month>/<int:region>/<int:salesperson>/
        http://haya244099.pythonanywhere.com/core/calculate-monthly-score/10/1/5
 
6.	Design fifth route allows searching for commissions of a salesperson according to a specific month and specific year and display the result in list (only administrator).
           according the id of salesperson to specific month 
          http://haya244099.pythonanywhere.com/core/sales-by-month/<int:salesperson>/?<int:month>
          http://haya244099.pythonanywhere.com/core/sales-by-month/5/?month=10
            according the id of salesperson to specific year
          http://haya244099.pythonanywhere.com/core/sales-by-year/<int:salesperson>/?<int:year>
          http://haya244099.pythonanywhere.com/core/sales-by-year/5/?year=10
             according the id of salesperson to specific  month & year 
          http://haya244099.pythonanywhere.com/core/sales-by-year_month/<int:salesperson>/?<int:month>/?<int:year>
          http://haya244099.pythonanywhere.com/core/sales-by-year_month/5/?month=10&year=2023


